<div>
    <img align="left" width="96" src="http://dooxe-creative.net/projects/jpx/assets/images/logo.png"/>
    <h1>jpx<br/>A javascript image processing toolkit</h1>    
</div>
<br/><br/>


## Documentation
* [Wiki](https://github.com/dooxe/jpx/wiki)
* [API documentation](http://dooxe-creative.net/projects/jpx/docs/api/index.html)

